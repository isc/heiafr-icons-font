# HEIA-FR Icons Font

This repository contains an Icon Font for the "Haute école d'ingénierie et d'architecture". The font
is produced using [IcoMoon](https://icomoon.io/). You can rebuild the font by importing 
the file [HEIAFR.json](https://gitlab.forge.hefr.ch/isc/heiafr-icons-font/-/blob/master/HEIAFR.json)
in IcoMoon.

**To see the font in action and for the list of all icons, look at the [demo page](https://isc.pages.forge.hefr.ch/heiafr-icons-font/).**

If you want to add more icons, just send me an [e-mail](mailto:jacques.supcik@hefr.ch) or send a _Merge Request_
with the updated `HEIAFR.json` file.
